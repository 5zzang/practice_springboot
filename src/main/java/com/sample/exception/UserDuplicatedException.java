package com.sample.exception;

import lombok.Getter;

/**
 * Created by 5zzang on 9/14/15.
 */
public class UserDuplicatedException extends RuntimeException {
    @Getter
    private String username;

    public UserDuplicatedException(String username) {
        this.username = username;
    }
}
