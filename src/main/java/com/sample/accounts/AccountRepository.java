package com.sample.accounts;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Taewoo, Kim(Michael) (5zzang@gmail.com)
 * @since 14-09-2015
 */
public interface AccountRepository extends JpaRepository<Account, Long> {
    Account findByUsername(String username);
}
